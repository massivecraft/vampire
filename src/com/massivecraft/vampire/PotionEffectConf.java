package com.massivecraft.vampire;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventPriority;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class PotionEffectConf
{
	// -------------------------------------------- //
	// FIELDS
	// -------------------------------------------- //
	
	public EventPriority priority;
	
	public boolean colorSet;
	public int colorValue;
	
	public Map<String, Integer> effectIdToStrength;
	
	// -------------------------------------------- //
	// CONSTRUCT
	// -------------------------------------------- //
	
	public PotionEffectConf(EventPriority priority, boolean colorSet, int colorValue, Map<String, Integer> effectToStrength)
	{
		this.priority = priority;
		
		this.colorSet = colorSet;
		this.colorValue = colorValue;
		
		this.effectIdToStrength = effectToStrength;
	}
	
	public PotionEffectConf()
	{
		this(EventPriority.NORMAL, false, 0, new HashMap<>());
	}
	
	// -------------------------------------------- //
	// UTIL
	// -------------------------------------------- //
	
	public void addPotionEffects(LivingEntity entity, int targetDuration, int okDuration)
	{
		Map<String, Integer> effectIdToStrength = new HashMap<>(this.effectIdToStrength);
		
		for (PotionEffect pe : entity.getActivePotionEffects())
		{
			// We may update if the duration is below the ok duration
			if (pe.getDuration() < okDuration) continue;
			
			// Is there even an entry to remove?
			String key = pe.getType().getName();
			Integer peStrength = effectIdToStrength.get(key);
			if (peStrength == null) continue;
			if (peStrength < 1) continue;
			
			// We may update if the strength is different
			int apeStrength = pe.getAmplifier();
			if (peStrength != apeStrength) continue;
			
			// Otherwise we may not
			effectIdToStrength.remove(key);
		}
		
		for (Entry<String, Integer> entry : effectIdToStrength.entrySet())
		{
			PotionEffectType pet = PotionEffectType.getByName(entry.getKey());
			if (pet == null) continue;
			Integer strength = entry.getValue();
			
			entity.addPotionEffect(new PotionEffect(pet, targetDuration, strength, true), true);
		}
	}
	
	public void removePotionEffects(LivingEntity entity)
	{
		// The keys to deactivate
		Set<String> keys = new HashSet<>(this.effectIdToStrength.keySet());
	
		// The currently active ids 
		Set<String> activeIds = new HashSet<>();
		for (PotionEffect pe : entity.getActivePotionEffects())
		{
			activeIds.add(pe.getType().getName());
		}
		
		// It only makes sense to deactivate active ids.
		Iterator<String> iter = keys.iterator();
		while(iter.hasNext())
		{
			String key = iter.next();
			if (activeIds.contains(key)) continue;
			iter.remove();
		}
		
		// Perform the remove
		for (String key : keys)
		{
			PotionEffectType type = PotionEffectType.getByName(key);
			if (type != null) entity.removePotionEffect(type);
		}
	}
	
}
