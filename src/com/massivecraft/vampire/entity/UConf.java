package com.massivecraft.vampire.entity;

import com.massivecraft.massivecore.collections.BackstringSet;
import com.massivecraft.massivecore.store.Entity;
import com.massivecraft.massivecore.util.MUtil;
import com.massivecraft.vampire.PotionEffectConf;
import com.massivecraft.vampire.altar.AltarDark;
import com.massivecraft.vampire.altar.AltarLight;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UConf extends Entity<UConf>
{
	// -------------------------------------------- //
	// META
	// -------------------------------------------- //
	
	public static UConf get(Object oid)
	{
		return UConfColls.get().get2(oid);
	}
	
	// -------------------------------------------- //
	// FX
	// -------------------------------------------- //
	
	public double fxSmokePerMilli = 8D / 1000D; // 8 smokes per second
	public double fxEnderPerMilli = 2D / 1000D; // 2 enders per second
	public int fxEnderRandomMaxLen = 1;
	public double fxSmokeBurstCount = 30D;
	public double fxFlameBurstCount = 5D;
	public double fxEnderBurstCount = 3D;
	
	// -------------------------------------------- //
	// SHRIEK
	// -------------------------------------------- //
	
	public long shriekWaitMessageCooldownMillis = 500;
	public long shriekCooldownMillis = 30 * 1000;
	
	// -------------------------------------------- //
	// MISC
	// -------------------------------------------- //
	
	public Set<DamageCause> blockDamageFrom = MUtil.set(
		DamageCause.DROWNING,
		DamageCause.FALL,
		DamageCause.STARVATION
	);
	
	public Set<RegainReason> blockHealthFrom = MUtil.set(
		RegainReason.SATIATED,
		RegainReason.REGEN	
	);
	
	// -------------------------------------------- //
	// UPDATE
	// -------------------------------------------- //
	
	public Map<String,Boolean> updatePermsVampire = MUtil.map(
		"vampire.is.vampire", true,
		"vampire.is.human", false
	);
	
	public Map<String,Boolean> updatePermsHuman = MUtil.map(
		"vampire.is.vampire", false,
		"vampire.is.human", true
	);

	public int updateRespawnFood = 20;
	public int updateRespawnHealth = 20;
	
	public Boolean updateNameColor = false;
	public ChatColor updateNameColorTo = ChatColor.RED;
	
	// -------------------------------------------- //
	// DROP SELF
	// -------------------------------------------- //
	
	public Set<Material> dropSelfMaterials = MUtil.set(
		Material.COBWEB,
		Material.GLOWSTONE,
		Material.BOOKSHELF,
		Material.DEAD_BUSH
	);
	
	// -------------------------------------------- //
	// BLOODLUST
	// -------------------------------------------- //
	
	public double bloodlustMinFood = 2.5D;
	public double bloodlustFoodPerMilli = -20D / (60D * 1000D); // You can bloodlust for 60 seconds
	public double bloodlustSmokes = 1.5D;
	
	// -------------------------------------------- //
	// NIGHTVISION
	// -------------------------------------------- //
	
	public boolean nightvisionCanBeUsed = true;
	
	// -------------------------------------------- //
	// UNDEAD HORSES
	// -------------------------------------------- //
	
	public boolean canInfectHorses = true;
 
	// -------------------------------------------- //
	// POTION EFFECTS
	// -------------------------------------------- //
	
	public PotionEffectConf effectConfBloodlust = new PotionEffectConf(EventPriority.HIGHEST, true, 0x1f1f23, MUtil.map(
		"JUMP", 1,
		"SPEED", 1
	));

	public PotionEffectConf effectConfNightvision = new PotionEffectConf(EventPriority.HIGH, true, 0, MUtil.map(
		"NIGHT_VISION", 1
	));

	public PotionEffectConf effectConfVampire = new PotionEffectConf(EventPriority.NORMAL, true, 0, MUtil.map(
		"JUMP", 0,
		"SPEED", 1
	));
	
	public PotionEffectConf effectConfInfected = new PotionEffectConf(EventPriority.NORMAL, true, 0x587653, new HashMap<>());
	
	public PotionEffectConf effectConfHuman = new PotionEffectConf(EventPriority.NORMAL, false, 0, new HashMap<>());

	
	// -------------------------------------------- //
	// REGEN
	// -------------------------------------------- //
	
	public double regenMinFood = 2.5D;
	public int regenDelayMillis = 10*1000;
	public double regenFoodPerMilli = 0.5D / 1000D; // Regen 0.5 food per second
	public double regenHealthPerFood = 2D;
	
	// -------------------------------------------- //
	// TRUCE
	// -------------------------------------------- //
	
	// One minute
	public long truceBreakMillis = 60L * 1000L; 
	
	// These are the creature types that won't target vampires
	public BackstringSet<EntityType> truceEntityTypes = new BackstringSet<>(EntityType.class,
		"BLAZE",
		"CAVE_SPIDER",
		"CREEPER",
		"ENDERMAN",
		"GHAST",
		"GIANT",
		"MAGMA_CUBE",
		"PIG_ZOMBIE",
		"SKELETON",
		"SPIDER",
		"ZOMBIE",
		"WITCH",
		"GUARDIAN",
		"SILVERFISH",
		"ENDERMITE"
	);
	
	// -------------------------------------------- //
	// COMBAT
	// -------------------------------------------- //
	
	public double combatDamageFactorWithoutBloodlust = 1.0;
	public double combatDamageFactorWithBloodlust = 1.15;
	
	private final static transient int damageDiamondSword = 7;
	public int combatWoodDamage = 3*damageDiamondSword;
	
	public Set<Material> combatWoodMaterials = MUtil.set(
		Material.WOODEN_AXE,
		Material.WOODEN_HOE,
		Material.WOODEN_PICKAXE,
		Material.WOODEN_SHOVEL,
		Material.WOODEN_SWORD,
		Material.STICK,
		Material.TORCH,
		Material.REDSTONE_TORCH,
		Material.SIGN,
		Material.ACACIA_FENCE,
		Material.BIRCH_FENCE,
		Material.DARK_OAK_FENCE,
		Material.JUNGLE_FENCE,
		Material.OAK_FENCE,
		Material.SPRUCE_FENCE,
		Material.ACACIA_FENCE_GATE,
		Material.BIRCH_FENCE_GATE,
		Material.DARK_OAK_FENCE_GATE,
		Material.JUNGLE_FENCE_GATE,
		Material.OAK_FENCE_GATE,
		Material.SPRUCE_FENCE_GATE
	);
	
	// -------------------------------------------- //
	// INFECTION
	// -------------------------------------------- //
	
	// It will take you 0.25h to turn
	public double infectionPerMilli = 0.25D / (1000D * 60D * 60D);
	
	public int infectionProgressNauseaTicks = 12*20;
	public int infectionProgressDamage = 1;
	
	// We have disabled this feature per default.
	// public Double infectionRiskAtCloseCombatWithoutIntent = 0.003;
	// public Double infectionRiskAtCloseCombatWithIntent = 0.05;
	public Double infectionRiskAtCloseCombatWithoutIntent = 0D;
	public Double infectionRiskAtCloseCombatWithIntent = 0D;
	
	// -------------------------------------------- //
	// TRADE
	// -------------------------------------------- //
	
	public double tradeOfferMaxDistance = 2d;
	public long tradeOfferToleranceMillis = 20 * 1000;
	public double tradeVisualDistance = 7D;
	public double tradePercentage = 1d;

	// -------------------------------------------- //
	// FOOD
	// -------------------------------------------- //
	
	public boolean foodCakeAllowed = true;
	
	public Map<EntityType, Double> entityTypeFullFoodQuotient = MUtil.map(
		EntityType.ENDER_DRAGON, 140/20D,
		EntityType.MUSHROOM_COW, 20/20D,
		EntityType.GIANT, 50/20D,
		EntityType.CREEPER, 10/20D,
		EntityType.SKELETON, 10/20D,
		EntityType.SPIDER, 10/20D,
		EntityType.ZOMBIE, 10/20D,
		EntityType.SLIME, 10/20D,
		EntityType.GHAST, 10/20D,
		EntityType.PIG_ZOMBIE, 10/20D,
		EntityType.ENDERMAN, 10/20D,
		EntityType.CAVE_SPIDER, 10/20D,
		EntityType.SILVERFISH, 10/20D,
		EntityType.BLAZE, 10/20D,
		EntityType.MAGMA_CUBE, 10/20D,
		EntityType.PIG, 10/20D,
		EntityType.SHEEP, 10/20D,
		EntityType.COW, 10/20D,
		EntityType.HORSE, 10/20D,
		EntityType.CHICKEN, 10/20D,
		EntityType.SQUID, 10/20D,
		EntityType.OCELOT, 10/20D,
		EntityType.IRON_GOLEM, 10/20D,
		EntityType.VILLAGER, 10/20D,
		EntityType.PLAYER, 10/20D,
		EntityType.WOLF, 0/20D,
		EntityType.SNOWMAN, 0/20D
	);

	// -------------------------------------------- //
	// HOLY WATER
	// -------------------------------------------- //
	
	public double holyWaterSplashRadius = 6D;
	public double holyWaterTemp = 0.7D;
	
	public List<ItemStack> holyWaterResources = MUtil.list(
		new ItemStack(Material.POTION, 1),
		new ItemStack(Material.LAPIS_LAZULI, 1)
	);
	
	// -------------------------------------------- //
	// SUN
	// -------------------------------------------- //
	
	public double opacityPerArmorPiece = 0.125d;
	public double baseRad = -0.2d;
	public double tempPerRadAndMilli = 1d / (10d * 1000d); // it should take 10 seconds to reach max temp in maximum sunlight.
	
	public double sunNauseaTemp = 0.20d;
	public double sunWeaknessTemp = 0.30d;
	public double sunSlowTemp = 0.50d;
	public double sunBlindnessTemp = 0.80d;
	public double sunBurnTemp = 0.90d;
	
	public int sunNauseaTicks = 10*20;
	public int sunWeaknessTicks = 10*20;
	public int sunSlowTicks = 10*20;
	public int sunBlindnessTicks = 10*20;
	public int sunBurnTicks = 3*20;	
	
	public double sunSmokesPerTempAndMilli = 12D / 1000D; // 12 smokes per second in full sunlight
	public double sunFlamesPerTempAndMilli = 0.4D / 1000D; // 0.4 flames every second in full sunlight
	
	//We assume opacity 1 for all materials not in this map
	private final static transient Double AIR = 0D;
	private final static transient Double GROUND = 0D;
	private final static transient Double STAIRS = 1D;
	private final static transient Double SLABS = 1D;
	private final static transient Double DOOR = 0D;
	private final static transient Double THIN = 0D;
	private final static transient Double STICK = 0.1D;
	private final static transient Double WATER = 0.2D;
	private final static transient Double VEGETATION = 0.2D;
	private final static transient Double FENCE = 0.4D;
	private final static transient Double GLASS = 0.5D;
	
	public Map<Material,Double> typeOpacity = MUtil.map(
		Material.AIR, AIR,
		
		Material.ACACIA_SAPLING, VEGETATION,
		Material.BIRCH_SAPLING, VEGETATION,
		Material.DARK_OAK_SAPLING, VEGETATION,
		Material.JUNGLE_SAPLING, VEGETATION,
		Material.OAK_SAPLING, VEGETATION,
		Material.SPRUCE_SAPLING, VEGETATION,
		Material.ACACIA_LEAVES, VEGETATION,
		Material.BIRCH_LEAVES, VEGETATION,
		Material.DARK_OAK_LEAVES, VEGETATION,
		Material.JUNGLE_LEAVES, VEGETATION,
		Material.OAK_LEAVES, VEGETATION,
		Material.SPRUCE_LEAVES, VEGETATION,
		//Material.CORNFLOWER, VEGETATION,
		//Material.LILY_OF_THE_VALLEY, VEGETATION,
		//Material.WITHER_ROSE, VEGETATION,
		Material.ALLIUM, VEGETATION,
		Material.AZURE_BLUET, VEGETATION,
		Material.BLUE_ORCHID, VEGETATION,
		Material.DANDELION, VEGETATION,
		Material.DEAD_BUSH, VEGETATION,
		Material.FERN, VEGETATION,
		Material.GRASS, VEGETATION,
		Material.LARGE_FERN, VEGETATION,
		Material.LILAC, VEGETATION,
		Material.ORANGE_TULIP, VEGETATION,
		Material.OXEYE_DAISY, VEGETATION,
		Material.PEONY, VEGETATION,
		Material.PINK_TULIP, VEGETATION,
		Material.POPPY, VEGETATION,
		Material.RED_TULIP, VEGETATION,
		Material.ROSE_BUSH, VEGETATION,
		Material.SUNFLOWER, VEGETATION,
		Material.TALL_GRASS, VEGETATION,
		Material.WHITE_TULIP, VEGETATION,
		Material.BROWN_MUSHROOM, VEGETATION,
		Material.RED_MUSHROOM, VEGETATION,
		Material.BEETROOTS, VEGETATION,
		Material.CARROTS, VEGETATION,
		Material.LILY_PAD, VEGETATION,
		Material.ATTACHED_MELON_STEM, VEGETATION,
		Material.MELON_STEM, VEGETATION,
		Material.NETHER_WART, VEGETATION,
		Material.POTATOES, VEGETATION,
		Material.ATTACHED_PUMPKIN_STEM, VEGETATION,
		Material.PUMPKIN_STEM, VEGETATION,
		Material.SUGAR_CANE, VEGETATION,
		Material.VINE, VEGETATION,
		Material.WHEAT, VEGETATION,
		
		Material.WATER, WATER,
		
		Material.GLASS, GLASS,
		Material.BLACK_STAINED_GLASS, GLASS,
		Material.BLUE_STAINED_GLASS, GLASS,
		Material.BROWN_STAINED_GLASS, GLASS,
		Material.CYAN_STAINED_GLASS, GLASS,
		Material.GRAY_STAINED_GLASS, GLASS,
		Material.GREEN_STAINED_GLASS, GLASS,
		Material.LIGHT_BLUE_STAINED_GLASS, GLASS,
		Material.LIGHT_GRAY_STAINED_GLASS, GLASS,
		Material.LIME_STAINED_GLASS, GLASS,
		Material.MAGENTA_STAINED_GLASS, GLASS,
		Material.ORANGE_STAINED_GLASS, GLASS,
		Material.PINK_STAINED_GLASS, GLASS,
		Material.PURPLE_STAINED_GLASS, GLASS,
		Material.RED_STAINED_GLASS, GLASS,
		Material.WHITE_STAINED_GLASS, GLASS,
		Material.YELLOW_STAINED_GLASS, GLASS,
		
		Material.COMPARATOR, GROUND,
		Material.DETECTOR_RAIL, GROUND,
		Material.POWERED_RAIL, GROUND,
		Material.REDSTONE, GROUND,
		Material.REDSTONE_WIRE, GROUND,
		Material.REPEATER, GROUND,
		Material.ACACIA_PRESSURE_PLATE, GROUND,
		Material.BIRCH_PRESSURE_PLATE, GROUND,
		Material.DARK_OAK_PRESSURE_PLATE, GROUND,
		Material.HEAVY_WEIGHTED_PRESSURE_PLATE, GROUND,
		Material.JUNGLE_PRESSURE_PLATE, GROUND,
		Material.LIGHT_WEIGHTED_PRESSURE_PLATE, GROUND,
		Material.OAK_PRESSURE_PLATE, GROUND,
		Material.SPRUCE_PRESSURE_PLATE, GROUND,
		Material.STONE_PRESSURE_PLATE, GROUND,
		Material.SNOW, GROUND,
		
		Material.COBWEB, THIN,
		Material.FIRE, THIN,
		Material.LADDER, THIN,
		Material.LEVER, THIN,
		Material.ACACIA_BUTTON, THIN,
		Material.BIRCH_BUTTON, THIN,
		Material.DARK_OAK_BUTTON, THIN,
		Material.JUNGLE_BUTTON, THIN,
		Material.OAK_BUTTON, THIN,
		Material.SPRUCE_BUTTON, THIN,
		Material.STONE_BUTTON, THIN,
		Material.GLASS_PANE, THIN,
		Material.BLACK_STAINED_GLASS_PANE, THIN,
		Material.BLUE_STAINED_GLASS_PANE, THIN,
		Material.BROWN_STAINED_GLASS_PANE, THIN,
		Material.CYAN_STAINED_GLASS_PANE, THIN,
		Material.GRAY_STAINED_GLASS_PANE, THIN,
		Material.GREEN_STAINED_GLASS_PANE, THIN,
		Material.LIGHT_BLUE_STAINED_GLASS_PANE, THIN,
		Material.LIGHT_GRAY_STAINED_GLASS_PANE, THIN,
		Material.LIME_STAINED_GLASS_PANE, THIN,
		Material.MAGENTA_STAINED_GLASS_PANE, THIN,
		Material.ORANGE_STAINED_GLASS_PANE, THIN,
		Material.PINK_STAINED_GLASS_PANE, THIN,
		Material.PURPLE_STAINED_GLASS_PANE, THIN,
		Material.RED_STAINED_GLASS_PANE, THIN,
		Material.WHITE_STAINED_GLASS_PANE, THIN,
		Material.YELLOW_STAINED_GLASS_PANE, THIN,
		
		Material.ACACIA_SLAB, SLABS,
		Material.BIRCH_SLAB, SLABS,
		Material.BRICK_SLAB, SLABS,
		Material.COBBLESTONE_SLAB, SLABS,
		Material.DARK_OAK_SLAB, SLABS,
		Material.DARK_PRISMARINE_SLAB, SLABS,
		Material.JUNGLE_SLAB, SLABS,
		Material.NETHER_BRICK_SLAB, SLABS,
		Material.OAK_SLAB, SLABS,
		Material.PETRIFIED_OAK_SLAB, SLABS,
		Material.PRISMARINE_BRICK_SLAB, SLABS,
		Material.PRISMARINE_SLAB, SLABS,
		Material.PURPUR_SLAB, SLABS,
		Material.QUARTZ_SLAB, SLABS,
		Material.RED_SANDSTONE_SLAB, SLABS,
		Material.SANDSTONE_SLAB, SLABS,
		Material.SPRUCE_SLAB, SLABS,
		Material.STONE_BRICK_SLAB, SLABS,
		Material.STONE_SLAB, SLABS,
		
		Material.ACACIA_STAIRS, STAIRS,
		Material.BIRCH_STAIRS, STAIRS,
		Material.BRICK_STAIRS, STAIRS,
		Material.COBBLESTONE_STAIRS, STAIRS,
		Material.DARK_OAK_STAIRS, STAIRS,
		Material.DARK_PRISMARINE_STAIRS, STAIRS,
		Material.JUNGLE_STAIRS, STAIRS,
		Material.NETHER_BRICK_STAIRS, STAIRS,
		Material.OAK_STAIRS, STAIRS,
		Material.PRISMARINE_BRICK_STAIRS, STAIRS,
		Material.PRISMARINE_STAIRS, STAIRS,
		Material.PURPUR_STAIRS, STAIRS,
		Material.QUARTZ_STAIRS, STAIRS,
		Material.RED_SANDSTONE_STAIRS, STAIRS,
		Material.SANDSTONE_STAIRS, STAIRS,
		Material.SPRUCE_STAIRS, STAIRS,
		Material.STONE_BRICK_STAIRS, STAIRS,
		
		Material.TORCH, STICK,
		Material.WALL_TORCH, STICK,
		Material.REDSTONE_TORCH, STICK,
		Material.REDSTONE_WALL_TORCH, STICK,
		Material.SIGN, STICK,
		Material.WALL_SIGN, STICK,
		
		Material.DARK_OAK_DOOR, DOOR,
		Material.ACACIA_DOOR, DOOR,
		Material.BIRCH_DOOR, DOOR,
		Material.IRON_DOOR, DOOR,
		Material.JUNGLE_DOOR, DOOR,
		Material.OAK_DOOR, DOOR,
		Material.SPRUCE_DOOR, DOOR,
		Material.ACACIA_TRAPDOOR, DOOR,
		Material.BIRCH_TRAPDOOR, DOOR,
		Material.DARK_OAK_TRAPDOOR, DOOR,
		Material.IRON_TRAPDOOR, DOOR,
		Material.JUNGLE_TRAPDOOR, DOOR,
		Material.OAK_TRAPDOOR, DOOR,
		Material.SPRUCE_TRAPDOOR, DOOR,
		Material.ACACIA_FENCE_GATE, DOOR,
		Material.BIRCH_FENCE_GATE, DOOR,
		Material.DARK_OAK_FENCE_GATE, DOOR,
		Material.JUNGLE_FENCE_GATE, DOOR,
		Material.OAK_FENCE_GATE, DOOR,
		Material.SPRUCE_FENCE_GATE, DOOR,
		
		Material.ACACIA_FENCE, FENCE,
		Material.BIRCH_FENCE, FENCE,
		Material.DARK_OAK_FENCE, FENCE,
		Material.JUNGLE_FENCE, FENCE,
		Material.NETHER_BRICK_FENCE, FENCE,
		Material.OAK_FENCE, FENCE,
		Material.SPRUCE_FENCE, FENCE
	);
	
	// -------------------------------------------- //
	// ALTARS
	// -------------------------------------------- //
	
	public int altarSearchRadius = 10;
	public double altarMinRatioForInfo = 0;
	
	public AltarDark altarDark = new AltarDark();
	public AltarLight altarLight = new AltarLight();	
	
}
